#include "vl53l0x_i2c_platform.h"
#include "stm32f4xx.h"
#include "Timer.h"
#include "string.h"

#include "diag/Trace.h"

#define I2Cx            I2C1
#define I2C_TIMEOUT		(5000) // 5s
I2C_HandleTypeDef I2cHandle;

int32_t VL53L0X_comms_initialise(uint8_t  comms_type, uint16_t comms_speed_khz)
{
	if ( comms_type == I2C ) {
		I2cHandle.Instance             = I2Cx;

		I2cHandle.Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
		I2cHandle.Init.ClockSpeed      = comms_speed_khz * 100;
		I2cHandle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
		I2cHandle.Init.DutyCycle       = I2C_DUTYCYCLE_2;
		I2cHandle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
		I2cHandle.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLE;

		if ( HAL_I2C_Init(&I2cHandle) != HAL_OK ) {
			return STATUS_FAIL;
		}
		return STATUS_OK;
	}
	return STATUS_FAIL;
}

int32_t VL53L0X_comms_close(void)
{
	if ( HAL_I2C_DeInit(&I2cHandle) != HAL_OK ) {
		return STATUS_FAIL;
	}
	return STATUS_OK;
}

int32_t VL53L0X_cycle_power(void)
{
	return STATUS_OK;
}

int32_t VL53L0X_write_multi(uint8_t address, uint8_t index, uint8_t  *pdata, int32_t count)
{
    uint8_t buf[10] = {0};	//FIXME
    buf[0] = index;
    memmove( buf + 1, pdata, count );

	if ( HAL_I2C_Master_Transmit(&I2cHandle, (uint16_t)address, (uint8_t*)buf, (uint16_t)count + 1, I2C_TIMEOUT) != HAL_OK ) {
		return STATUS_FAIL;
	}
	return STATUS_OK;
}

int32_t VL53L0X_read_multi(uint8_t addr,  uint8_t index, uint8_t  *pdata, int32_t count)
{
	uint8_t address = addr;

	if ( HAL_I2C_Master_Transmit(&I2cHandle, (uint16_t)address, (uint8_t*)&index, (uint16_t)1, I2C_TIMEOUT) != HAL_OK ) {
		return STATUS_FAIL;
	}
	if ( HAL_I2C_Master_Receive(&I2cHandle, (uint16_t)address, pdata, (uint16_t)count, I2C_TIMEOUT) != HAL_OK ) {
		return STATUS_FAIL;
	}
	return STATUS_OK;
}

int32_t VL53L0X_write_byte(uint8_t address,  uint8_t index, uint8_t   data)
{
	return VL53L0X_write_multi(address, index, (uint8_t*)&data, 1);
}

int32_t VL53L0X_write_word(uint8_t address,  uint8_t index, uint16_t  data)
{
	int32_t status = STATUS_OK;
	uint8_t  buffer[BYTES_PER_WORD];

	// Split 16-bit word into MS and LS uint8_t
	buffer[0] = (uint8_t)(data >> 8);
	buffer[1] = (uint8_t)(data &  0x00FF);

	if(index%2 == 1)
	{
		status = VL53L0X_write_multi(address, index, &buffer[0], 1);
		status = VL53L0X_write_multi(address, index + 1, &buffer[1], 1);
		// serial comms cannot handle word writes to non 2-byte aligned registers.
	}
	else
	{
		status = VL53L0X_write_multi(address, index, buffer, BYTES_PER_WORD);
	}

	return status;
}

int32_t VL53L0X_write_dword(uint8_t address, uint8_t index, uint32_t  data)
{
    int32_t status = STATUS_OK;
    uint8_t  buffer[BYTES_PER_DWORD];

    // Split 32-bit word into MS ... LS bytes
    buffer[0] = (uint8_t) (data >> 24);
    buffer[1] = (uint8_t)((data &  0x00FF0000) >> 16);
    buffer[2] = (uint8_t)((data &  0x0000FF00) >> 8);
    buffer[3] = (uint8_t) (data &  0x000000FF);

    status = VL53L0X_write_multi(address, index, buffer, BYTES_PER_DWORD);

    return status;
}

int32_t VL53L0X_read_byte(uint8_t address,  uint8_t index, uint8_t  *pdata)
{
	return VL53L0X_read_multi(address, index, pdata, 1);
}

int32_t VL53L0X_read_word(uint8_t address,  uint8_t index, uint16_t *pdata)
{
    int32_t  status = STATUS_OK;
	uint8_t  buffer[BYTES_PER_WORD];

    status = VL53L0X_read_multi(address, index, buffer, BYTES_PER_WORD);
	*pdata = ((uint16_t)buffer[0]<<8) + (uint16_t)buffer[1];

    return status;
}

int32_t VL53L0X_read_dword(uint8_t address, uint8_t index, uint32_t *pdata)
{
    int32_t status = STATUS_OK;
	uint8_t  buffer[BYTES_PER_DWORD];

    status = VL53L0X_read_multi(address, index, buffer, BYTES_PER_DWORD);
    *pdata = ((uint32_t)buffer[0]<<24) + ((uint32_t)buffer[1]<<16) + ((uint32_t)buffer[2]<<8) + (uint32_t)buffer[3];

    return status;
}

int32_t VL53L0X_platform_wait_us(int32_t wait_us)
{
    (void) wait_us; // Unused parameter
	return STATUS_OK;
}

int32_t VL53L0X_wait_ms(int32_t wait_ms)
{
    timer_sleep(wait_ms);
	return STATUS_OK;
}

int32_t VL53L0X_set_gpio(uint8_t  level)
{
    (void) level; // Unused parameter
	return STATUS_OK;
}

int32_t VL53L0X_get_gpio(uint8_t *plevel)
{
    (void) plevel; // Unused parameter
	return STATUS_OK;
}

int32_t VL53L0X_release_gpio(void)
{
	return STATUS_OK;
}

int32_t VL53L0X_get_timer_frequency(int32_t *ptimer_freq_hz)
{
    (void) ptimer_freq_hz; // Unused parameter
	return STATUS_OK;
}

int32_t VL53L0X_get_timer_value(int32_t *ptimer_count)
{
    (void) ptimer_count; // Unused parameter
	return STATUS_OK;
}
